import testinfra
def test_os_release(host):
    assert host.file("/etc/os-release").contains("Alpine Linux")
def test_sshd_inactive(host):
    assert host.service("sshd").is_running is False
def test_packages(host):
    pkg = host.package("nginx")
    assert pkg.is_installed
    assert pkg.version.startswith("1.16.1")
